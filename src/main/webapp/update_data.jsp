<%@ page import="java.sql.Connection" %>
<%@ page import="org.capitalc.web.db.MySQL" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: Dilhara
  Date: 4/1/2023
  Time: 10:53 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update User Data</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 text-center">
                    <label class="text-black fw-bold text-decoration-underline fs-1">Update User Data</label>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-4 mx-auto">
                            <div class="row">
                                <%
                                    if (request.getParameter("user_id") != "") {
                                        String id = request.getParameter("user_id");
                                        Connection connection1 = null;
                                        try {
                                            connection1 = MySQL.getConnection();
                                            ResultSet resultSet = connection1.createStatement().executeQuery("SELECT * FROM `user` WHERE `id` = '" + id + "'");
                                            if (resultSet.next()) {
                                                String fname = resultSet.getString("fname");
                                                String lname = resultSet.getString("lname");
                                                String email = resultSet.getString("email");
                                %>
                                <form action="update_data" method="post">
                                    <div class="col-12 mt-2">
                                        <input type="hidden" name="user_id" value="<%=id%>">
                                        <label>First Name</label>
                                        <input type="text" name="fname" value="<%=fname%>" class="form-control"/>
                                    </div>
                                    <div class="col-12 mt-2">
                                        <label>Last Name</label>
                                        <input type="text" name="lname" value="<%=lname%>" class="form-control"/>
                                    </div>
                                    <div class="col-12 mt-2">
                                        <label>Email Address</label>
                                        <input type="text" name="email" value="<%=email%>" class="form-control"/>
                                    </div>
                                    <div class="col-12 mt-2 text-center">
                                        <button class="btn btn-primary">Update User Data</button>
                                    </div>
                                </form>
                                <%
                                            }
                                        } catch (Exception e) {

                                        } finally {
                                            if (connection1 != null) {
                                                try {
                                                    connection1.close();
                                                } catch (SQLException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                %>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
