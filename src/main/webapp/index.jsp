<%@ page import="java.sql.Connection" %>
<%@ page import="org.capitalc.web.db.MySQL" %><%--
  Created by IntelliJ IDEA.
  User: Dilhara
  Date: 3/31/2023
  Time: 1:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Web Task 01</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 text-center">
                    <label class="text-black fw-bold text-decoration-underline fs-1">Enter User Data</label>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-4 mx-auto">
                            <div class="row">
                                <form action="add_data" method="post">
                                    <div class="col-12 mt-2">
                                        <label>First Name</label>
                                        <input type="text" name="fname" class="form-control"/>
                                    </div>
                                    <div class="col-12 mt-2">
                                        <label>Last Name</label>
                                        <input type="text" name="lname" class="form-control"/>
                                    </div>
                                    <div class="col-12 mt-2">
                                        <label>Email Address</label>
                                        <input type="text" name="email" class="form-control"/>
                                    </div>
                                    <div class="col-12 mt-2 text-center">
                                        <button class="btn btn-primary">Add User Data</button>
                                    </div>
                                    <div class="col-12 text-start mt-2">
                                        <a href="view_data.jsp" class="btn px-2 py-1 bg-secondary text-white">View User Data</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
