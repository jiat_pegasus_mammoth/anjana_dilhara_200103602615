<%@ page import="org.capitalc.web.db.MySQL" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: Dilhara
  Date: 4/1/2023
  Time: 1:20 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View User Data</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 text-center my-2">
                    <label class="fw-bold fs-1 text-decoration-underline">User Data View & Update</label>
                </div>
            </div>
            <div class="row">
                <div class="col-6 mx-auto">
                    <div class="row">
                        <div class="col-12 text-start mt-2">
                            <a href="index.jsp" class="btn px-2 py-1 bg-secondary text-white">Add User Data</a>
                        </div>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Email Address</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <%
                            Connection connection = null;
                            try {
                                connection = MySQL.getConnection();
                                ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM `user`");
                                int i = 1;
                                while (rs.next()) { %>
                        <tr>
                            <th><%=i%>
                            </th>
                            <td><%=rs.getString("fname")%>
                            </td>
                            <td><%=rs.getString("lname")%>
                            </td>
                            <td><%=rs.getString("email")%>
                            </td>
                            <td>
                                <a href="update_data.jsp?user_id=<%=rs.getString("id")%>"
                                   class="btn btn-success btn-sm mx-2"><i class="bi bi-pencil-square"></i></a>
                                <a href="delete_data?user_id=<%=rs.getString("id")%>"
                                   class="btn btn-danger btn-sm mx-2"><i
                                        class="bi bi-trash-fill"></i></a>
                            </td>
                        </tr>
                        <%
                                    i++;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                if (connection != null) {
                                    try {
                                        connection.close();
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        %>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>


</body>
</html>
<%
    Connection connection1 = null;
    try {
        connection1 = MySQL.getConnection();
        String user_id = request.getParameter("user_id");
        System.out.println(user_id);
    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        if (connection1 != null) {
            try {
                connection1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
%>