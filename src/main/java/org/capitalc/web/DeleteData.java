package org.capitalc.web;

import org.capitalc.web.db.MySQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "delete_data", urlPatterns = "/delete_data")
public class DeleteData extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("user_id");
        Connection connection = null;
        try {
            connection = MySQL.getConnection();
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM `user` WHERE `id` = '" + id + "'");
            if (rs.next()) {
                connection.createStatement().executeUpdate("DELETE FROM `user` WHERE `id` = '" + id + "'");
                response.getWriter().println("<script>alert('User data deleted!');window.location='view_data.jsp'</script>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
