package org.capitalc.web;

import org.capitalc.web.db.MySQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "update_data", urlPatterns = "/update_data")
public class UpdateData extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("user_id");
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");
//        System.out.println(id);
        Connection connection = null;
        try {
            connection = MySQL.getConnection();
            connection.createStatement().executeUpdate("UPDATE `user` SET `fname`='" + fname + "' , `lname`='" + lname + "' , `email`='" + email + "' WHERE `id`= '" + id + "'");
            response.getWriter().print("<script>alert('User data updated!');window.location='view_data.jsp';</script>");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
