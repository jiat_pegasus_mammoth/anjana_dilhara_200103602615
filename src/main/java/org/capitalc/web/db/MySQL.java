package org.capitalc.web.db;


import org.capitalc.web.util.ApplicationProperties;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySQL {
    private static Connection connection;

    public static Connection getConnection() throws Exception {
        ApplicationProperties properties = ApplicationProperties.getInstance();
        Class.forName(properties.get("sql.connection.driver"));
        connection = DriverManager.getConnection(properties.get("sql.connection.url"), properties.get("sql.connection.root"), properties.get("sql.connection.password"));
        return connection;
    }
}
