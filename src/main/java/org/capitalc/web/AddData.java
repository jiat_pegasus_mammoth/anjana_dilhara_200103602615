package org.capitalc.web;

import org.capitalc.web.db.MySQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "add_data", urlPatterns = "/add_data")
public class AddData extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");
        Connection connection = null;
        try {
            connection = MySQL.getConnection();
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM `user` WHERE `email` = '" + email + "'");
            if (rs.next()) {
                response.getWriter().print("<script type=\"text/javascript\">alert('This email address already inserted!');" +
                        "window.location='index.jsp';</script>");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO `user`(`fname`,`lname`,`email`) VALUES ('" + fname + "','" + lname + "','" + email + "')");
                response.sendRedirect("index.jsp");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
